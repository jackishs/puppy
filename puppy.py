import jinja2
import pandas as pd
import os
import json

#############################################################################################
#############################    Cleaning Data    ###########################################
#############################################################################################

#create tmp files
os.makedirs('tmp', exist_ok=True)

firebase='https://puppy-peru-dev.web.app'
def wsp_comprar(single_path):
    import urllib.parse
    phone='51921482286'
    #firebase='https://puppy-peru-dev.web.app'
    txt=f'Hola *Puppy*, quiero comprar esta zapatilla:\n{firebase}/{single_path}'
    
    msg=f'https://api.whatsapp.com/send?phone={phone}&text=' + urllib.parse.quote(txt, safe='')
    return msg  

def descuento(fila):
    dsct=(fila['precio_normal']-fila['precio_oferta'])*(100/fila['precio_normal'])
    return round(dsct)

#Read data, Cleaning - Save it as json files 
xl = pd.ExcelFile('data/puppy.xlsx')
for sheet_name in xl.sheet_names:  # see all sheet names
    df=xl.parse(sheet_name)  # read a specific sheet to DataFrame

    #Cleaning Data
    for col in ['marca_modelo','colores','genero','descripcion']:
        df[f'{col}']=df[f'{col}'].str.strip()
        df[f'{col}']=df[f'{col}'].str.replace('  ',' ')

    df['tallas']=df['tallas'].str.replace(' ','').str.replace(',',' / ')
    df['fotos']=df['fotos'].str.replace(' ','').str.split(',')
    df['fotos']=df['fotos'].apply(lambda l: l[::-1]) #reverse list
    df['fotos']=df['fotos'].apply(lambda l: [f'data/{sheet_name}/{x}.png' for x in l])
    df['caracteristicas']=df['caracteristicas'].str.strip().str.split(',')
    
    #add/build new data
    df['id']= [x for x in range(1,len(df)+1)]
    df['single_path']=df['id'].apply(lambda x: f'{sheet_name}_{x}.html')
    df['wsp_comprar']=df['single_path'].apply(wsp_comprar)
    df['firebase_url']=df['single_path'].apply(lambda x: f'{firebase}/{x}')
    try:
        df['descuento']=df.apply(descuento,axis=1)
    except:
        pass

    #save as json file
    df.to_json(f'tmp/{sheet_name}.json', orient = 'records',indent=4)   


#############################################################################################
###########################    Build Base htmls    ##########################################
#############################################################################################

# Ofertas_div.html
outputfile = 'tmp/ofertas_div.html'
with open('tmp/ofertas.json') as json_file:
    data = json.load(json_file)

subs = jinja2.Environment( 
              loader=jinja2.FileSystemLoader('./')      
              ).get_template('template/ofertas_div.html').render(data=data)

with open(outputfile,'w',encoding='utf-8') as f: f.write(subs)

# index.html
outputfile = 'index.html'
with open(f'tmp/novedades.json') as json_file:
    input = json.load(json_file)
    data=[input[i:i+4] for i in range(0, len(input), 4)]

subs = jinja2.Environment( 
              loader=jinja2.FileSystemLoader('./')      
              ).get_template('template/index.html').render(data=data)

with open(outputfile,'w',encoding='utf-8') as f: f.write(subs)

# ubicanos.html
outputfile = 'ubicanos.html'
subs = jinja2.Environment( 
              loader=jinja2.FileSystemLoader('./')      
              ).get_template('template/mail.html').render()

with open(outputfile,'w',encoding='utf-8') as f: f.write(subs)


#############################################################################################
###########################    Build Products htmls    ######################################
#############################################################################################

#Build all products.html
xl = pd.ExcelFile('data/puppy.xlsx')
for sheet_name in xl.sheet_names:
    with open(f'tmp/{sheet_name}.json') as json_file:
        input = json.load(json_file)
        data=[input[i:i+3] for i in range(0, len(input), 3)]

    outputfile = f'{sheet_name}.html'
    subs = jinja2.Environment( 
                loader=jinja2.FileSystemLoader('./')      
                ).get_template('template/products.html').render(data=data, sheet_name=sheet_name)
    
    with open(outputfile,'w',encoding='utf-8') as f: f.write(subs)


#############################################################################################
###########################    Build Single htmls    ########################################
#############################################################################################

#Build all single htmls
xl = pd.ExcelFile('data/puppy.xlsx')
for sheet_name in xl.sheet_names: 
    #sheet_name='ofertas'
    with open(f'tmp/{sheet_name}.json') as json_file:
        data = json.load(json_file)

    for dt in data:
        outputfile = f'{sheet_name}_{dt["id"]}.html'
        subs = jinja2.Environment( 
                    loader=jinja2.FileSystemLoader('./')      
                    ).get_template('template/single.html').render(dt)
        # lets write the substitution to a file
        with open(outputfile,'w',encoding='utf-8') as f: f.write(subs)
