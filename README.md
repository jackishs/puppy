﻿# PuppyWeb

# Configuración:
1. Instalar Python 3.8 [link](https://www.python.org/ftp/python/3.8.10/python-3.8.10-amd64.exe)
2. Instalar git [link](https://github.com/git-for-windows/git/releases/download/v2.31.1.windows.1/Git-2.31.1-64-bit.exe)

3. Instalar paquetes de python
```bash
pip install jinja2==3.0.0 pandas==1.2.4 openpyxl==3.0.7
```

4. Configurar git
```bash
git config --global user.email "jackis.hs9@gmail.com"
```
```bash
git config --global user.name "jackishs"
```
   
# Descargar el script
1. Descargar el repositorio
```bash
git clone https://jackishs:N3Qx43czj4Z8ZMPYjgrD@gitlab.com/jackishs/puppy.git
```

2. Ejecutar python
```bash
python puppy.py
````

3. Actializar cambios
```bash
git add .
```
```bash
git commit -m "dd-mm-aa"
```
```bash
git push
```  